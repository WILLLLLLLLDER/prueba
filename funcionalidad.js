var operandoa;
var operandob;
var operacion;
function init(){
    //aqui estan las variables
    var resultado = document.getElementById("resultado");
    var reset = document.getElementById("reset");
    var suma = document.getElementById("suma");
    var resta = document.getElementById("resta");
    var multiplicasion = document.getElementById("multiplicasion");
    var division = document.getElementById("division");
    var igual = document.getElementById("igual");
    var uno = document.getElementById("uno");
    var dos = document.getElementById("dos");
    var tres = document.getElementById("tres");
    var cuatro = document.getElementById("cuatro");
    var cinco = document.getElementById("cinco");
    var seis = document.getElementById("seis");
    var siete = document.getElementById("siete");
    var ocho = document.getElementById("ocho");
    var nueve = document.getElementById("nueve");
    var cero = document.getElementById("cero");
    var suma = document.getElementById("suma");
    var seno = document.getElementById("seno");
    var coseno = document.getElementById("coseno");
    var tangente = document.getElementById("tangente");
    var raiz = document.getElementById("raiz");
    var potencia = document.getElementById("potencia");
    var parentesisiz = document.getElementById("parentesisiz");
    var parentesisder = document.getElementById("parentesisder");
    var punto = document.getElementById("punto");
    //estos son los eventos
    uno.onclick = function(e){
        resultado.textContent = resultado.textContent + "1";   
    }
    dos.onclick = function(e){
        resultado.textContent = resultado.textContent + "2";   
    }
    tres.onclick = function(e){
        resultado.textContent = resultado.textContent + "3";   
    }
    cuatro.onclick = function(e){
        resultado.textContent = resultado.textContent + "4";   
    }
    cinco.onclick = function(e){
        resultado.textContent = resultado.textContent + "5";   
    }
    seis.onclick = function(e){
        resultado.textContent = resultado.textContent + "6";   
    }
    siete.onclick = function(e){
        resultado.textContent = resultado.textContent + "7";   
    }
    ocho.onclick = function(e){
        resultado.textContent = resultado.textContent + "8";   
    }
    nueve.onclick = function(e){
        resultado.textContent = resultado.textContent + "9";   
    }
    cero.onclick = function(e){
        resultado.textContent = resultado.textContent + "0";   
    }
    punto.onclick = function(e){
        resultado.textContent = resultado.textContent + ".";   
    }
    potencia.onclick = function(e){
        resultado.textContent = resultado.textContent + "^";   
    }  
    raiz.onclick = function(e){
        resultado.textContent = resultado.textContent + "√";   
    } 
    coseno.onclick = function(e){
        resultado.textContent = resultado.textContent + "cos";   
    }
    seno.onclick = function(e){
        resultado.textContent = resultado.textContent + "sen";   
    }
    tangente.onclick = function(e){
        resultado.textContent = resultado.textContent + "tan";   
    }
    reset.onclick = function(e){
        resetear();  
    }
    suma.onclick = function(e){
        operandoa = resultado.textContent;
        operacion ="+";
        limpiar();
    }
    resta.onclick = function(e){
        operandoa = resultado.textContent;
        operacion ="-";
        limpiar();
    }
    multiplicasion.onclick = function(e){
        operandoa = resultado.textContent;
        operacion ="x";
        limpiar();
    }
    division.onclick = function(e){
        operandoa = resultado.textContent;
        operacion ="/";
        limpiar();
    }
    potencia.onclick = function(e){
        operandoa = resultado.textContent;
        operacion ="^";
        limpiar();
    }
    raiz.onclick = function(e){
        operandoa = resultado.textContent;
        operacion ="√";
        limpiar();
    }
    coseno.onclick = function(e){
        operandoa = resultado.textContent;
        operacion ="cos";
        limpiar();
    }
    seno.onclick = function(e){
        operandoa = resultado.textContent;
        operacion ="sen";
        limpiar();
    }
    tangente.onclick = function(e){
        operandoa = resultado.textContent;
        operacion ="tan";
        limpiar();
    }
    igual.onclick = function(e){
        operandob = resultado.textContent;
        resolver();
    }
}
function limpiar(){
    resultado.textContent = "";
}
function resetear(){
    resultado.textContent = "";
    operandoa = 0;
    operandob = 0;
    operacion = "";
}
function resolver(){
     var res = 0;
    switch(operacion){
        case "+":
        res = parseFloat(operandoa) + parseFloat(operandob);
            break;
        case "-":
        res = parseFloat(operandoa) - parseFloat(operandob);
        break;
        case "x":
        res = parseFloat(operandoa) * parseFloat(operandob);
        break;
        case "/":
        res = parseFloat(operandoa) / parseFloat(operandob);
        break;
        case "^":
        res = Math.pow(parseFloat(operandoa),parseFloat(operandob));
        break;
        case "√":
        res = Math.sqrt(parseFloat(operandoa));
        break;
        case "cos":
        res = Math.cos(parseFloat(operandoa)*Math.PI/180);
        break;
        case "sen":
        res = Math.sin(parseFloat(operandoa)*Math.PI/180);
        break;
        case "tan":
        res = Math.tan(parseFloat(operandoa)*Math.PI/180);
        break;
    }
    resetear();
    resultado.textContent = res;
}
